<?php
date_default_timezone_set('Asia/Jakarta');
?>
<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
        <link href="https://fonts.googleapis.com/css?family=Seymour+One&display=swap" rel="stylesheet">


        <title>DeskClock</title>
        <style type="text/css">
        body{
            font-family: 'Seymour One', sans-serif;
        }
        #icon {
            font-size: 100px;
            color: white;
        }
        contents {
            padding: 2px;
        }
        </style>
    </head>
    <body onload="startTime()">
        <div class="container-fluid">
            <div class="row" style="height: 100vh">
                <div style="background-color: blue;" class="text-center col-8 p-2 h-100">
                    <div class="justify-content-center" style="margin-top: 20%">
                        <h1 style="font-size: 10vw;color: white;" id="txt"></h1>
                        @ozkula, Rozaq Isnaini Nugraha
                    </div>
                </div>
                <div style="background-color: #cc0000 " class="col-4 p-2">
                    
                    <div class="text-center container">
                        <div style="color: white;margin-top: 20%">
                            <div><img src="https://lh3.googleusercontent.com/-0NDmSjr2DmE/Whu484iFWbI/AAAAAAAAD_I/XnaQl5k0FY00I21w0thvguZvrb85QND4ACEwYBhgL/w140-h139-p/rin.png"></div>
                            <div><canvas id="icon" width="100" height="128"></canvas></div>
                            <div id="tempC">Loading...</div>
                            <div id="tempF"></div>
                            <div id="locationName"></div>
                        </div>
                        <script src="https://rawgithub.com/darkskyapp/skycons/master/skycons.js"></script>
                    </div>
                </div>
            </div>
            <script>
            function startTime() {
                var today = new Date();
                var h = today.getHours();
                var m = today.getMinutes();
                var s = today.getSeconds();
                m = checkTime(m);
                s = checkTime(s);
                document.getElementById('txt').innerHTML = h + ":" + m + ":" + s;
                var t = setTimeout(startTime, 500);
            }

            function checkTime(i) {
                if (i < 10) {
                    i = "0" + i
                }; // add zero in front of numbers < 10
                return i;
            }

            function getWeather(lat, lon) {
                $.ajax({
                    url: "https://api.darksky.net/forecast/9c4e8944261ed3b6f5f3438431a5cfa0/" + lat + "," + lon,
                    dataType: "jsonp",
                    success: function(data) {
                        console.log("Current temp: " + data.currently.temperature);
                        // get all the information
                        var fahrenheit = data.currently.temperature.toFixed(2),
                            locationName = data.timezone,
                            splice = locationName.indexOf("/"),
                            icon = data.currently.icon.toUpperCase()
                        console.log(icon)
                        console.log(splice)
                        //apparently the temperature is returned in kelvin, so we need to quickly convert it to celsius, which gives a horrible number so is rounded to two decimal places.
                        var celsius = ((fahrenheit - 32) * 5 / 9).toFixed(2); // Who in the name of all that is holy uses Fahrenheit?
                        $("#tempC").html(celsius + "&deg;C");
                        $("#locationName").html(locationName.substring(splice + 1).replace("_", " "));
                        var icons = new Skycons({
                            "color": "white"
                        });
                        icons.set("icon", icon)
                        icons.play();
                        //Alters all of the elements to display the information to the user.
                    }
                });
            }
            // Gets the users position
            if (navigator.geolocation) { // Gets the users position
                navigator.geolocation.getCurrentPosition(function(position) {
                    $("#data").html("latitude: " + position.coords.latitude + "<br>longitude: " + position.coords.longitude);
                    var lat = position.coords.latitude;
                    var lon = position.coords.longitude;
                    getWeather(lat, lon); // Calls the getWeather function which is defined above. Passes two arguments which are the user's current lattitude and longitude.
                });
            }
            // If there is no location the code stalls and temp is replaced with a message telling the user to turn location on!
            // API key ""2ca252d74a373acc1b59fe6e45e5d3ae""
            </script>
        </body>
    </html>